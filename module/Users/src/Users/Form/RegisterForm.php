<?php

namespace Users\Form;

use Zend\Form\Form;

class RegisterForm extends Form {
      
    public function __construct($name = null) {
        parent::__construct('Register');
        $this->setAttribute('method', 'post');
        
        $this->add([
            'name' => 'nombre',
            'attributes' => ['type' => 'text', 'class' => 'form-control'],
            'options' => array(
                'label' => 'Tu nombre',
                'label_attributes' => ['class' => 'col-sm-2 control-label']
                )
        ]);
        
        $this->add([
            'name' => 'email',
            'attributes' => [ 'type' => 'email', 'required' => 'required' ],
            'options' => array(
                'label' => 'Correo Electronico',
                'label_atributes' => ['class' => 'col-sm-2 control-label']
                )
        ]);
        
        
    }
    
}
