<?php

namespace Users\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Users\Form\RegisterForm;

class RegisterController extends AbstractActionController {
 
    public function indexAction() {
        $this->_data['form'] = new RegisterForm();
        $this->_data['titulo'] = "Register Form";
        return new ViewModel($this->_data);
    }
    
    public function confirmAction() {
        $viewModel = new ViewModel();
        return $viewModel;
    }
 
    private $_data = array();
}